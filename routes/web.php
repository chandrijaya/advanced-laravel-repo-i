<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['roleMiddleware', 'auth'])->group(function(){
    Route::get('/route1', 'TestController@superadmin')->name('superadmin');
    Route::get('/route2', 'TestController@admin')->name('admin');
    Route::get('/route3', 'TestController@guest')->name('guest');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
