<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if ($request->route()->uri() == 'route1') {
            if (Auth::user()->isRole() == 'SuperAdmin') {
                return $next($request);
            }

            else {
                abort(403);
            }
        } elseif ($request->route()->uri() == 'route2') {
            if (Auth::user()->isRole() != 'Guest' ) {
                return $next($request);
            }

            else {
                abort(403);
            }
        } else {
            return $next($request);
        }     
    }
}
