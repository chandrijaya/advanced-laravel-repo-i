<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function superadmin() {
        return "Anda adalah Super Admin";
    }

    public function admin() {
        return "Anda adalah Admin";
    }

    public function guest() {
        return "Anda adalah Guest";
    }
}
